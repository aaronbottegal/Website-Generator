#!/bin/bash
set -e

./GenSite.sh #Gen the site on this folder.
    if [ $? -ne 0 ];then
        echo "Failed to generate site $OurDir"
        exit 1
    fi

read -p "Site generated. Push changes to local server for testing? (y/N) " -n 1 -r
echo #(optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo "Copy files to the server here"
    #Something like:
    #sudo rm -rf /srv/http/Example/
    #sudo mkdir /srv/http/Example
    #sudo rsync -ar ./Output_nogit/ /srv/http/Example/
    #Local server testing only, move WIP folder to the server.
    #sudo rsync -ar --exclude '.gitignore' './HTML Sources/include/WIP/' /srv/http/Example/
fi

read -p "Push changes to web server? (y/N) " -n 1 -r
echo #(optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo "Put your sync code here."
    #Something like:
    #rsync -avzr --no-perms --no-owner --no-group --delete -e "ssh -p [PORT HERE]" ./Output_nogit/ [USER]@[SERVER IP]:[LOCATION]
fi
