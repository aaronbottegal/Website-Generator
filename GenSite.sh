#!/usr/bin/env bash

#Uses JQ to parse json in this file.

set -e

#Process all folders given to us.
FILE=${PWD##*/}
echo "Generating site $FILE..."
OutVar=$(jq .output_dir ./Data.json | sed "s/^\([\"']\)\(.*\)\1\$/\2/g")
if [ "$OutVar" == "null" ]; then
    echo "Error: No output directory specified. Add output_dir to Data.json"
    exit 1
fi
rm -rf ./$OutVar
OutDir=$OutVar
SiteVar=$(jq .output_site ./Data.json | sed "s/^\([\"']\)\(.*\)\1\$/\2/g")
if [ "$SiteVar" != "null" ]; then
    OutDir+=$SiteVar
fi
mkdir -p $OutDir
mkdir -p $OutDir/Media/
mkdir -p $OutDir/Scripts/
mkdir -p $OutDir/Styles/
cp -r ./Template/Media/ ./$OutDir/
cp -r ./Template/Scripts/ ./$OutDir/
cp -r ./Template/Styles/ ./$OutDir/
rm -f ./HTML\ Sources/parts/*.part*
rm -f ./HTML\ Sources/parts/*.json
./ParseTemplate.py
./GenSiteV2.py
if [ $? ]; then
    echo "Generating site $FILE completed."
    if [ -d './HTML Sources/include' ]; then
        echo "Moving other necessary files into place."
        rsync -pa --exclude '/WIP' --exclude '.gitignore' './HTML Sources/include/' ./$OutVar/
    fi
    exit 0
else
    echo "Generating site $FILE FAILED."
    exit 1
fi
cd ../