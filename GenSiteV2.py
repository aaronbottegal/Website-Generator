#!/usr/bin/python3
import sys, os, errno
import json

class Gengine():

    def Header():
        global pageinfo
        global outputfile
        OutputFile("Header",None,None)
        return

    def Navbar():
        global pageinfo
        global galleryinfo
        global websitedata
        global InsertOverride

        OutputFile("Navbar","pre",None)

        for Link in websitedata['pages']:
            #404 page not linked.
            if Link.get('linktext') == None:
                continue
            #Otherwise insert this link info.
            InsertOverride = Link
            if pageinfo != Link or galleryinfo != None:
                OutputFile("Navbar","default",None)
            else:
                OutputFile("Navbar","selected",None)

        OutputFile("Navbar","post",None)
        return

    def Content():
        OutputFile("Content",None,None)
        return

    def Footer():
        global pageinfo
        global galleryinfo
        OutputFile("Footer",None,None)
        pass

    def Insert(String):
        global pageinfo
        global galleryinfo
        global outputfile
        global InsertOverride
        global SocialLoop

        if String == "Title":
            #Pick the most accurate title.
            if galleryinfo == None:
                outputfile.write(pageinfo['title'])
            else:
                outputfile.write(galleryinfo['title'])

        if String == "LinkText":
            Text=InsertOverride['linktext']
            outputfile.write(Text)

        if String == "LinkURL":
            Text=InsertOverride['url']
            outputfile.write(Text)

        if String=="Content":
            if galleryinfo == None:
                Page=pageinfo['url']
            else:
                Page=galleryinfo['url']
            with open("./HTML Sources/"+Page,"r") as infile:
                for line in infile:
                    outputfile.write(line)

        return

    def OpenOut():
        global OutputDirectory
        global websitedata
        global outputfile
        #Clear file by opening for writing, in case of outdated data.
        if websitedata.get('output_dir'):
            OutputDirectory = websitedata.get('output_dir')
            if websitedata.get('output_site'):
                OutputDirectory += websitedata.get('output_site')
        else:
            sys.exit("Need output_dir value in json data.")
        try:
            os.makedirs("./"+OutputDirectory)
        except OSError as e:
            if e.errno != errno.EEXIST:
                sys.exit("Failed to make output directory folder. Check storage space and permissions.")
        if galleryinfo == None:
            outputfile = open("./"+OutputDirectory+pageinfo['url'],'w')
        else:
            outputfile = open("./"+OutputDirectory+galleryinfo['url'],'w')
        return

    def CloseOut():
        #Done with file.
        global outputfile
        outputfile.close()
        return


#Outputs a part defined in the template.
#Part name and ID, or a string.
def OutputFile(name, id, String):
    global outputfile
    global templatemap
    if String != None:
        pass
    else:
        entry = None
        for search in templatemap:
            if search.get('name')==name and search.get('id')==id:
                entry=search
                break;
        if entry == None:
            print("No part found matching",name,id)
            return
        with open("HTML Sources/parts/Template.part"+str(search['index']),"r") as Temp:
            buf = ''
            while True:
                char = Temp.read(1)
                if char == '':
                    #Write remaining buffer.
                    outputfile.write(buf)
                    #EOF
                    break
                buf += char
                if len(buf) > 4:
                    #Write extra char
                    outputfile.write(buf[0])
                    buf = buf[1:]
                #We have 
                if buf == "<ins":
                    buf = ''
                    while True:
                        #Extract data
                        char = Temp.read(1)
                        if char == '/':
                            #Extract >
                            Temp.read(1)
                            break
                        elif char == '>':
                            break
                        else:
                            buf += char
                    buf=buf.split('"')
                    buf=buf[1]
                    Gengine.Insert(buf)
                    buf=''

def MakePage(PageNum):
    global pageinfo
    global galleryinfo
    global SocialLoop

    #Per-page counters.
    SocialLoop=0

    Gengine.OpenOut()
    if PageNum >= 0:
        print("\tMaking subpage",galleryinfo['url'])
        if galleryinfo.get('steps'):
            for DynFuncs in galleryinfo['steps']:
                FunctionToCall = getattr(globals()['Gengine'],DynFuncs)
                FunctionToCall()
    elif pageinfo.get('steps'):
        galleryinfo = None
        if pageinfo.get('steps'):
            for DynFuncs in pageinfo['steps']:
                FunctionToCall = getattr(globals()['Gengine'],DynFuncs)
                FunctionToCall()
    Gengine.CloseOut()
    return

if __name__ == '__main__':
    global websitedata
    global templatemap
    global pageinfo
    global galleryinfo
    with open("HTML Sources/parts/TemplateMap.json") as templatemapfile:
        templatemap = json.load(templatemapfile)
        with open('Data.json', 'r') as websitedatafile:
            websitedata = json.load(websitedatafile)
            for page in websitedata['pages']:
                galleryinfo = None
                if page.get('url'):
                    print("Generating page {}".format(page['url']))
                    pageinfo = page
                    MakePage(-1)
                if page.get('sublinks'):
                    loop=0
                    print("Generating sublinks {}".format(page['sublinks'],loop))
                    for gallerypage in websitedata[page['sublinks']]:
                        galleryinfo = gallerypage
                        if galleryinfo.get('url'):
                            MakePage(loop)
                        loop+=1