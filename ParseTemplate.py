#!/usr/bin/python3
import sys, os, errno
import json

def Parse():
    global OutFile
    #Find a part
    #Parts currently have to have their own newline as we don't have an easy
    #way to unread a file one character, find a solution later to make it work
    #more dynamically. Maybe even write it in C or something?
    OutFile = None
    Data = json.load(open("Data.json"))
    PartNum = 0
    Processing = 0
    OutData = []
    for templates in Data['Templates']:
        if OutFile != None:
            OutFile.close()
            OutFile = None
        with open(templates, "r") as Temp:
            Parts = []
            buf = ''
            while True:
                char = Temp.read(1)
                if char == '':
                    #EOF
                    if OutFile != None:
                        OutFile.write(buf)
                        OutFile.close()
                        OutFile = None
                    break
                buf += char
                if len(buf) > 5:
                    #If we have an open file, write the data.
                    if OutFile != None:
                        OutFile.write(buf[0])
                    buf = buf[1:]
                #We have all chars
                if buf == "<part":
                    if OutFile != None:
                        OutFile.close()
                        OutFile = None
                    OutFile = open("./HTML Sources/parts/Template.part"+str(PartNum),"w")
                    PartNum += 1
                    buf = ''
                    while True:
                        #Extract data
                        char = Temp.read(1)
                        if char == '/':
                            #Extract >
                            Temp.read(1)
                            #newline (optional)
                            Temp.read(1)
                            break
                        elif char == '>':
                            Temp.read(1)
                            break
                        else:
                            buf += char
                    Parts.append(buf)
                    buf = ''
            if 'OutFile' in locals():
                OutFile.write(buf)
            print("Seperating data into TemplateMap.json...")
            while len(Parts) > 0:
                Obj = {}
                Extract = Parts.pop(0)
                Extract = Extract.split('"')
                while True:
                    if Extract[0]==' name=':
                        del Extract[0]
                        Obj['name']=Extract[0]
                        del Extract[0]
                    elif Extract[0]==' id=':
                        del Extract[0]
                        Obj['id']=Extract[0]
                        del Extract[0]
                    else:
                        #print(Extract)
                        #No match or no more.
                        break
                Obj['index']=Processing
                Processing+=1
                #print("Object: ",Obj)
                OutData.append(dict(Obj))
    OutFile = open("./HTML Sources/parts/TemplateMap.json","w")
    OutFile.write(json.dumps(OutData, indent=4, sort_keys=False))
    OutFile.close
    if PartNum == 0:
        print("WARNING: No parts found in templates.")
    return

if __name__ == '__main__':
    Parse()