# About this project

This project generates static websites from templates and per-page source files.

# What happens

There are many simple pieces of work, here. First, Data.json file has the main outline of the website in it. The Templates array in the root object contains paths to template files to consume.

output_dir in Data.json tells what to name the output folder, and the output_site in Data.json tells where to output the website inside the output_dir.

Per-page data in Data.json is pretty self-explainitory.

The "HTML Sources" directory contains the page content per-page.

The "HTML Sources/parts" directory is where the tamplate parts are stored.

The "HTML Sources/include" directory is copied over output_dir, this is a great place to store your htaccess files for the server that really aren't part of the website, but still important to have. This is to combat keeping them on the server and being screwed if your hosting provider loses your files in a catastrophe.

The "HTML Sources/include/WIP" directory is coped over your website just like the include directory, but only when pushed to the local server.

# Templates

The template folder holds your template files, providing a framework for the code for the site. It also holds the media on the site. The idea is to be able to keep a rough Template.html file for editing the template, and previewing your changes without necessarily generating sites each time. You can have multiple templates for different code parts, as is shown in the example.

# How to use
##### (On Linux)

Run the Generate.sh script. You will need python3 and jq.

# How to edit the site generated.

Your main file will be the Data.json file, along with the content in the template folder and the extra feature folders explained above.

# Changes from Website Generator V1

Changed from a central generator to a per-site python program. This gives more flexible generation and doesn't limit what other sites can do. Before, all websites generated would have the same fetaures, no more or less, so any extra data or features had to be ported to all sites using it. Since this generator version sits in the directory with the site, it is much easier to maintain as it has no dependencies or limits on features that can be added to a specific project.


# Todos

Grouping pages. multiple "sites" for a single generation.

General cleaning.